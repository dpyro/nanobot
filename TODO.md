TODO
====

* Accept url:port args
* ~~Persistent store~~
* ~~Karma rate limiting~~
* Track user by hostmask
  * User karma by hostmask
* Generic command handler
  * `.karma set`
  * `.karma get`
  * `.karma best|top`
  * `.karma worst|least|bottom`
  * `.karma merge` for user hostmasks
  * Aesthetic commands
    * `.aesthetic`
    * `.clap`
    * `.gay`
    * `.flip`
* Owner authorization
* Mirror pasted images
* URL title scraping
* Merge args, env vars, config files, defaults
* Emoji tracker
* Enable automatic redis persistence