#!/usr/bin/perl

use utf8;

use strict;
use warnings;
use feature 'unicode_strings';

use constant KEY => "karma";

binmode(STDIN, ":encoding(UTF-8)");
binmode(STDOUT, ":encoding(UTF-8)");

# https://redis.io/topics/protocol
sub resp_array {
    my $argc = scalar(@_);
    my $results = "*${argc}\r\n";
    foreach my $arg (@_) {
        my $len = bytes::length($arg);
        $results .= "\$${len}\r\n${arg}\r\n";
    }
    return $results;
}

while (<>) {
    chomp;
    my ($score, $name) = split(",", $_, 2);
    my $cmd = resp_array("ZADD", KEY, "NX", $score, $name);
    print $cmd;
}