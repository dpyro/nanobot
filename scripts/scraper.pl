#!/usr/bin/perl

use 5.012;
use utf8;

use strict;
use warnings;

use Encode qw(decode);
use File::Spec;
use File::Find;

use integer;

binmode(STDOUT, ":utf8");

sub trim {
    my ($s) = @_;
    $s =~ s/^\s+|\s+$//g;
    return $s;
}

sub find_karma {
    my ($file) = @_;
    my %results = ();
    open my $fh, "<", $file or die "Cannot open file $file $!";
    while (my $line = <$fh>) {
        my $line = Encode::decode('UTF-8', $line, Encode::FB_WARN);
        if ($line =~ /<\S*> \S+: (.+?)'s karma is now (-?\d+)/) {
            $results{$1} = $2;
        }
    }
    return %results;
}

my @files = ();

find(sub {
    push(@files, $File::Find::name) unless -d $File::Find::name;
}, @ARGV);

my @sorted_files = sort {
    my (undef, undef, $file_a) = File::Spec->splitpath($a);
    my (undef, undef, $file_b) = File::Spec->splitpath($b);
    return $a cmp $b;
} @files;
# foreach my $file (@sorted_files) {
#     print "$file\n";
# }

my %results = ();
for my $file (@sorted_files) {
    my %counts = find_karma($file);
    foreach my $key (keys %counts) {
        my $cleaned = lc trim($key);
        if (!exists($results{$cleaned})) {
            $results{$cleaned} = 0;
        }
        if (abs($counts{$key}) >= abs($results{$cleaned})) {
            $results{$cleaned} = $counts{$key};
        }
    }
}

my @keys = sort {CORE::fc($a) cmp CORE::fc($b)} (keys %results);

foreach my $key (@keys) {
    print "$results{$key},$key\n";
}
