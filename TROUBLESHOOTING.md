TROUBLESHOOTING
===============

Load current directory data into a local Redis instance:

```sh
docker run --rm -v "$(pwd)":/data -p 6379:6379 dpyro/redis-cell --appendonly yes
```