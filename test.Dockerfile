FROM rust

WORKDIR /root

COPY Cargo.lock Cargo.toml ./
COPY src/ ./src
COPY data/ ./data

RUN cargo test