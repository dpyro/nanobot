use futures::future::{self, Future};
use irc::client::data::User;
use irc::client::prelude::*;
use irc::client::PackedIrcClient;
use irc::error::IrcError;
use irc::proto::caps::Capability;
use irc::proto::colors::FormattedStringExt;
use std::borrow::Cow;
use tokio_core::reactor;

use crate::types::NanoError;
use crate::handlers::*;

pub type NanoHandler<'a> = fn(NanoBot, Message) -> Box<Future<Item = (), Error = NanoError> + 'a>;

pub static HANDLERS: &'static [NanoHandler] = &[join_after_umode_handler, karma_handler, user_alias_handler];

//     lazy_static! {
//         static ref re_terms: Regex   = Regex::new(r"(?P<name>.+)\?$").unwrap();
//         static ref re_command: Regex = Regex::new(r"\.(?P<name>\S+)\s*(?P<args>.*)").unwrap();
//     }

#[derive(Clone)]
pub struct NanoBot {
    pub redis_client: redis::Client,
    pub irc_client: IrcClient,
}

impl NanoBot {
    pub fn new<'a>(
        handle: reactor::Handle,
        config: &'a Config,
    ) -> Box<Future<Item = (Self, impl Future<Item = (), Error = NanoError>), Error = NanoError> + 'a>
    {
        let redis_str = config.get_option("redis").unwrap_or_default(); // TODO: add handling
        let redis_client = match redis::Client::open(redis_str) {
            Ok(x) => x,
            Err(e) => return Box::new(future::err(e).from_err()),
        };

        let fut_irc = match IrcClient::new_future(handle, config) {
            Err(e) => return Box::new(future::err(NanoError::IrcError(e))),
            Ok(fut) => fut
                .and_then(|PackedIrcClient(irc_client, fut)| {
                    irc_client.send_cap_req(&[Capability::MultiPrefix])?;
                    irc_client.identify()?;
                    Ok(PackedIrcClient(irc_client, fut))
                })
                .from_err(),
        };

        Box::new(fut_irc.map(
            |PackedIrcClient(irc_client, fut_irc)| {
                let bot = Self {
                    redis_client,
                    irc_client,
                };

                bot.with_future(fut_irc)
            },
        ))
    }

    fn with_future<'f>(
        self,
        fut_irc: impl Future<Item = (), Error = IrcError> + 'f,
    ) -> (Self, impl Future<Item = (), Error = NanoError> + 'f) {
        fn for_each_msg<'a>(
            bot: NanoBot,
            msg: Message,
        ) -> impl Future<Item = (), Error = NanoError> + 'a {
            let futs = HANDLERS.iter().map(move |h| {
                h(bot.clone(), msg.clone()).then(|result| {
                    match result {
                        Ok(_) => (),
                        Err(e) => eprintln!("Handler error: {}", e),
                    };
                    future::ok(())
                })
            });

            future::join_all(futs).map(|_| ())
        }

        let self_ = self.clone();
        let stream = self.irc_client.stream();
        let fut_stream = stream.map_err(NanoError::IrcError).for_each(move |msg| {
            print!("{}", msg.to_string());
            for_each_msg(self_.clone(), msg)
        });

        let fut = fut_stream
            .join(fut_irc.from_err())
            .map(|_| ());

        (self, fut)
    }

    pub fn find_user(&self, chan: &str, nick: &str) -> Option<User> {
        // TODO: ensure only 1 nick matches?
        if let Some(users) = self.irc_client.list_users(chan) {
            for user in users {
                if user.get_nickname() == nick {
                    return Some(user);
                }
            }
        }
        None
    }
}

pub fn normalize_text<'a>(text: &'a str) -> Cow<'a, str> {
    match text.strip_formatting() {
        Cow::Borrowed(s) => Cow::Borrowed(s.trim_end()),
        Cow::Owned(s) => Cow::Owned(s.trim_end().to_string()),
    }
}
