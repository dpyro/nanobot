use clap;
use futures::prelude::*;
use irc::error::IrcError;
use redis::RedisError;
use std::error;
use std::fmt;

#[derive(Debug)]
pub enum NanoError {
    IrcError(IrcError),
    RedisError(RedisError),
    IoError(std::io::Error),
    OtherError(String),
}

impl fmt::Display for NanoError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            NanoError::IrcError(e) => e.fmt(f),
            NanoError::RedisError(e) => e.fmt(f),
            NanoError::IoError(e) => e.fmt(f),
            NanoError::OtherError(s) => s.fmt(f),
        }
    }
}

impl error::Error for NanoError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            NanoError::IrcError(_) => None, // TODO: impl
            NanoError::RedisError(e) => e.source(),
            NanoError::IoError(e) => e.source(),
            NanoError::OtherError(_) => None,
        }
    }
}

impl From<IrcError> for NanoError {
    fn from(err: IrcError) -> Self {
        NanoError::IrcError(err)
    }
}

impl From<RedisError> for NanoError {
    fn from(err: RedisError) -> Self {
        NanoError::RedisError(err)
    }
}

impl From<std::io::Error> for NanoError {
    fn from(err: std::io::Error) -> Self {
        NanoError::IoError(err)
    }
}

pub type NanoFuture<T> = Box<Future<Item = T, Error = NanoError>>;
