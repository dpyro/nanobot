use if_chain::if_chain;
use irc::client::prelude::*;
use futures::future;

use crate::bot::NanoBot;
use crate::types::NanoError;

pub const USERS_ALIASES_KEY: &'static str = "users/aliases";

pub fn user_alias_handler<'a>(bot: NanoBot, msg: Message) -> Box<Future<Item = (), Error = NanoError> + 'a> {
    if_chain! {
        if let Command::PRIVMSG(ref target, ref _text) = msg.command;
        if let Some(nick) = msg.source_nickname();
        // println!("alias_handler: checking users for {}", nick);
        if let Some(user) = bot.find_user(target, nick);
        // TODO: do a whois lookup to let this pass when async is working
        // println!("found user {:?} for nick {}", user, nick);
        if let Some(username) = user.get_username();
        then {
            let key = format!("{}/{}", USERS_ALIASES_KEY, username);
            let nick_ = nick.to_string();
            return Box::new(
                bot.redis_client
                .get_async_connection()
                .and_then(move |conn| {
                    let mut cmd = redis::cmd("ZINCRBY");
                    cmd.arg(key).arg(1).arg(nick_)
                        .query_async::<_, f64>(conn)
                        .map(|_| ())
                })
                .from_err()
            );
        }
    }

    Box::new(future::ok(()))
}
