use crate::bot::NanoBot;
use irc::client::prelude::*;
use irc::client::data::User;
use if_chain::if_chain;
use futures::future;
// use futures::prelude::*;
use lazy_static::lazy_static;
use regex::Regex;
use redis::RedisError;

use crate::bot::normalize_text;
use crate::types::{NanoError, NanoFuture};

pub const KARMA_KEY: &'static str = "karma";
pub const KARMA_USER_THROTTLE_KEY: &'static str = "karma/users/last";
pub const KARMA_USER_THROTTLE_WINDOW: i64 = 60 * 15;
pub const KARMA_USER_THROTTLE_SIZE: i64 = 3;

#[derive(Clone, Debug, PartialEq)]
struct KarmaMatchResult {
    name: String,
    delta: i64,
}

impl<'a> KarmaMatchResult {
    pub fn is_query(&self) -> bool {
        self.delta == 0
    }

    pub fn clamp(self, value: i64) -> Self {
        let v = value.abs();
        let delta = match value.signum() {
            -1 => self.delta.max(-v),
            1 => self.delta.min(v),
            _ => self.delta,
        };

        Self {
            name: self.name,
            delta,
        }
    }
}

fn karma_match(text: String) -> Option<KarmaMatchResult> {
    lazy_static! {
        static ref re: Regex = Regex::new(r"^(?P<name>[^?]+?)(?P<delta>\+{2,}|-{2,}|\?)$").unwrap();
    }

    if_chain! {
        if let Some(caps) = re.captures(&text);
        if let (Some(name), Some(delta)) = (caps.name("name"), caps.name("delta"));
        then {
            let amount = match delta.as_str().chars().next() {
                Some('+') => 1,
                Some('-') => -1,
                Some('?') => 0,
                _ => unreachable!(),
            } * (delta.as_str().len() as i64 - 1);

            return Some(KarmaMatchResult {
                name: name.as_str().to_string(),
                delta: amount,
            });
        }
    }

    None
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum ThrottleResult {
    Proceed,
    RetryAfter(i64),
}

// TODO: handle requesting more tokens than peak
// TODO: flatten error types
fn karma_user_thottle_apply<'a>(
    bot: &NanoBot,
    name: String,
    count: i64,
) -> impl Future<Item = ThrottleResult, Error = RedisError> + 'a {
    bot.redis_client
        .get_async_connection()
        .and_then(move |conn| {
            let mut cmd = redis::cmd("CL.THROTTLE");
            cmd
                .arg(format!("{}/{}", KARMA_USER_THROTTLE_KEY, name))
                .arg(KARMA_USER_THROTTLE_SIZE)
                .arg(KARMA_USER_THROTTLE_SIZE)
                .arg(KARMA_USER_THROTTLE_WINDOW)
                .arg(count)
                .query_async::<_, (i64, i64, i64, i64, i64)>(conn).map(
                |(_conn, (result, _limit, _remaining, retry_after, _reset_after))| {
                    if result == 0 {
                        ThrottleResult::Proceed
                    } else {
                        ThrottleResult::RetryAfter(retry_after)
                    }
                }
            )
        })
}

pub fn karma_handler<'a>(bot: NanoBot, msg: Message) -> Box<Future<Item = (), Error = NanoError> + 'a> {
    if_chain! {
        if let Command::PRIVMSG(target, text) = msg.command;
        if let Some(prefix) = msg.prefix;
        let norm_text = normalize_text(&text);
        // TODO: use redis::transaction(bot.redis_conn, keys: &[K], func: F)
        if let Some(mat) = karma_match(norm_text.into_owned());
        then {
            let fut = do_karma(&bot, &prefix, mat)
                .then(move |result| match result {
                    Ok(Some(s)) => bot.irc_client.send_privmsg(target, s),
                    Ok(None) => Ok(()),
                    Err(e) => {
                        eprintln!("Error @ {}: {}", target, e);
                        bot.irc_client.send_notice(target, e)
                    }
                })
                .from_err();

            return Box::new(fut);
        }
    }

    Box::new(future::ok(()))
}

fn do_karma(bot: &NanoBot, prefix: &str, mat: KarmaMatchResult) -> NanoFuture<Option<String>> {
    let m = mat.clamp(KARMA_USER_THROTTLE_SIZE);
    let name_ = m.name.to_string();

    let user = User::new(prefix);
    let nick = user.get_nickname().to_owned();

    if m.is_query() {
        return Box::new(bot
            .get_karma(&m.name)
            .and_then(move |result| {
                match result {
                    None => Ok(None),
                    Some(score) => {
                        let text = format!("💓 {}: {} has {} karma.", nick, name_, score);
                        Ok(Some(text))
                    },
                }
            })
        );
    }

    let lname = m.name.to_lowercase();
    if lname == nick.to_lowercase() {
        let text = "😒 You cannot adjust your own karma.";
        return Box::new(future::err(NanoError::OtherError(text.to_string())));
    }

    let user_alias = get_user_alias(&user);

    let bot_ = bot.clone();
    let nick_ = nick.to_string();
    let delta = m.delta;

    debug_assert_ne!(m.delta, 0);
    let result = karma_user_thottle_apply(&bot, user_alias, m.delta.abs())
        .from_err()
        .and_then(move |throttle| {
            match throttle {
                ThrottleResult::Proceed => return Ok(bot_),
                ThrottleResult::RetryAfter(secs) => {
                    let dir = if delta.is_positive() { "give" } else { "take" };
                    let text = format!(
                        "⚠️ Error: You must wait {} more seconds to {} {} karma.",
                        secs, dir, delta.abs()
                    );
                    return Err(NanoError::OtherError(text));
                }
            }
        })
        .and_then(|b| b.redis_client.get_async_connection().from_err()) // TODO: pass b as sidecar?
        .and_then(move |conn| {
            let mut cmd = redis::cmd("ZINCRBY");
            cmd.arg(KARMA_KEY).arg(delta).arg(lname)
                .query_async::<_, f64>(conn)
                .from_err::<NanoError>()
                .map(move |(_, score)| {
                    let (dir, prep) = if delta.is_positive() {
                        ("gave", "to")
                    } else {
                        ("removed", "from")
                    };
                    let s = format!("💞 {} {} {} karma {} {} ({})", nick_, dir, delta.abs(), prep, name_, score as i64);
                    
                    Some(s)
                })

        });

    Box::new(result)
}

trait BotKarmaExt {
    fn get_karma(&self, name: &str) -> NanoFuture<Option<i64>>;
}

impl BotKarmaExt for NanoBot {
    fn get_karma(&self, name: &str) -> NanoFuture<Option<i64>> {
        let lname = name.to_lowercase();

        let fut = self.redis_client
            .get_async_connection()
            .and_then(|conn| {
                let mut cmd = redis::cmd("ZSCORE");
                cmd.arg(KARMA_KEY).arg(lname)
                    .query_async(conn)
            })
            .from_err()
            // https://stackoverflow.com/a/41139453/1440740
            .map(|(_conn, result): (_, Option<f64>)| result.map(|f| f.max(std::i64::MIN as f64).min(std::i64::MAX as f64).round() as i64));

        Box::new(fut)
    }
}

fn get_alias(prefix: &str) -> String {
    let user = User::new(prefix);
    get_user_alias(&user)
}

fn get_user_alias(user: &User) -> String {
    let nickify = || format!("nickname/{}", user.get_nickname());
    let userify = |s| format!("username/{}", s);
    user.get_username().map_or_else(nickify, userify)
}

#[cfg(test)]
mod test {
    #[test]
    fn karma_match_query() {
        let cases = vec![
            ("food?", Some("food")),
            ("what's this?", Some("what's this")),
            ("no? i don't", None),
            ("ha? ha?", None),
        ];

        for (test, expect) in cases {
            let mat = super::karma_match(test.to_string());
            match expect {
                Some(expect) => {
                    let m = mat.unwrap();
                    assert_eq!(m.name.as_str(), expect);
                    assert!(m.is_query());
                },
                None => assert_eq!(mat, None),
            }
        }
    }

    #[test]
    fn karma_match_valid() {
        let cases = vec![
            ("food++", "food", 1),
            ("food++++", "food", 3),
            ("food--", "food", -1),
            ("food----", "food", -3),
        ];

        for (test, expect, score) in cases {
            let m = super::karma_match(test.to_string()).unwrap();
            assert_eq!(m.name, expect);
            assert_eq!(m.delta, score);
        }
    }

    #[test]
    fn karma_match_invalid() {
        let cases = vec!["hello world", "C++ there", "haha+-", "cool--l"];

        for case in cases {
            let m = super::karma_match(case.to_string());
            assert!(m.is_none());
        }
    }
}