use futures::future::{self, Future};
use irc::client::prelude::*;
use irc::proto::{Command, Message, Mode, UserMode};
use crate::bot::NanoBot;
use crate::types::NanoError;

use if_chain::if_chain;

pub fn join_after_umode_handler<'a>(bot: NanoBot, msg: Message) -> Box<Future<Item = (), Error = NanoError> + 'a>  {
    if_chain! {
        if bot.irc_client.config().get_option("channels_ident").is_some();
        if let Command::Raw(raw, targets, mode_str) = msg.command;
        if raw == "MODE";
        if let (Some(target), Some(ref mode_str)) = (targets.first(), mode_str);
        if target == bot.irc_client.current_nickname();
        if let Ok(modes) = Mode::as_user_modes(mode_str);
        then {
            for mode in modes.iter().rev() {
                println!("mode check: {}", mode);
                match mode {
                    Mode::Plus(UserMode::Restricted, _) => return do_join(bot),
                    Mode::Minus(UserMode::Restricted, _) => break,
                    _ => (),
                }
            }
        }
    }

    Box::new(future::ok(()))
}

fn do_join<'a>(bot: NanoBot) -> Box<Future<Item = (), Error = NanoError> + 'a> {
    let config = bot.irc_client.config();

    if let Some(chans_opt) = config.get_option("channels_ident") {
        let chans = split_chan_str(chans_opt);
        for (chan, key) in chans {
            let c = chan.trim().to_string();
            let msg = Command::JOIN(c, key.map(str::to_string), None);
            if let Err(e) = bot.irc_client.send(msg) {
                eprintln!("Join error after identify: {}", e);
            }
        }
    }

    Box::new(future::ok(()))
}

fn split_chan_str(s: &str) -> Vec<(&str, Option<&str>)> {
    let mut result = Vec::new();

    let chans = s.split(",");
    for chan in chans {
        let mut sp_iter = chan.splitn(2, "=");
        if let Some(name) = sp_iter.next() {
            let trimmed_name = name.trim();

            if !trimmed_name.is_empty() {
                let key = sp_iter.next().map(str::trim);
                result.push((trimmed_name, key));
            }
        }
    }

    result
}


#[cfg(test)]
mod test {
    #[test]
    fn split_chan_str_test() {
        let cases = vec![
            ("", vec![]),
            ("#test", vec![("#test", None)]),
            ("#test=10", vec![("#test", Some("10"))]),
            ("#test=#test", vec![("#test", Some("#test"))]),
            (" #test = #test ", vec![("#test", Some("#test"))]),
            ("#ab,#bc", vec![("#ab", None), ("#bc", None)]),
            ("#ab=hi,#bc", vec![("#ab", Some("hi")), ("#bc", None)]),
            ("#ab,#bc=bye", vec![("#ab", None), ("#bc", Some("bye"))]),
            ("#ab=hi,#bc=bye", vec![("#ab", Some("hi")), ("#bc", Some("bye"))]),
        ];

        for (test, expect) in cases {
            let result = super::split_chan_str(test);
            assert_eq!(result, expect);
        }
    }
}
