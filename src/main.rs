#![recursion_limit="128"]

use clap::clap_app;

extern crate lazy_static;

use futures::future;
use futures::prelude::*;
use irc::client::data::Config;
use std::env;
use tokio_core::reactor::Core;

mod bot;
mod types;
mod handlers;

// const CONFIG_PATH: &'static str = "data/config.toml"; // TODO: automatic fallback

fn merge<T>(opts: Vec<Option<T>>) -> Option<T> {
    for opt in opts {
        if opt.is_some() {
            return opt;
        }
    }
    None
}

fn run(config: &Config) -> Result<(), types::NanoError> {
    let mut reactor = Core::new()?;

    let fut_bot = bot::NanoBot::new(reactor.handle(), config);

    fut_bot
        .and_then(|(_bot, fut)| future::result(reactor.run(fut)))
        .wait()
}

fn main() -> Result<(), types::NanoError> {
    let matches = clap_app!(app =>
        (author: "Sumant Manne <me@sumantmanne.com>")
        (about: "Add karma and terms to your IRC channel!")
        (@arg CONFIG: -c --config +takes_value "Path to a custom config file (TOML, JSON)")
        (@arg IRC: -i --irc +takes_value "IRC server to connect to")
        (@arg REDIS: -r --redis +takes_value "Redis url")
        (@arg TEST: -t --test "Just test the connections and exit")
    )
    .get_matches();

    let config_path = merge(vec![matches.value_of("CONFIG")]).unwrap_or("/etc/nanobot.toml");

    match Config::load(config_path) {
        Ok(config) => {
            let irc_url = merge(vec![
                matches.value_of("IRC").map(str::to_string),
                env::var("NANOBOT_IRC").ok(),
                config.server.clone(),
            ])
            .unwrap_or("localhost".to_string());

            println!("IRC Server: {}", &irc_url);

            let env_redis = env::var("NANOBOT_REDIS").ok();
            let redis_url: &str = merge(vec![
                matches.value_of("REDIS"),
                env_redis.as_ref().map(String::as_str),
                config.get_option("redis"),
            ])
            .unwrap_or("redis://localhost");

            println!("Redis Server: {}", &redis_url);

            let test = merge(vec![if matches.is_present("TEST") {
                Some(true)
            } else {
                None
            }])
            .unwrap_or(false);

            let test_val = if test { "true" } else { "false" }; // TODO: std function?

            let options_copy = match config.options {
                Some(ref options) => {
                    let mut copy = options.clone();
                    copy.insert("redis".to_owned(), redis_url.to_owned());
                    copy.insert("test".to_owned(), test_val.to_owned());
                    Some(copy)
                },
                None => None
            };

            let merged_config = Config {
                server: Some(irc_url),
                options: options_copy,
                ..config
            };

            run(&merged_config)
        }
        Err(e) => {
            eprintln!("Config ({}) loading error: {}", config_path, e);
            Err(types::NanoError::IrcError(e))
        }
    }
}
