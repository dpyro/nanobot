FROM rust AS builder

WORKDIR /root

COPY Cargo.lock Cargo.toml ./
COPY src/ ./src
COPY data/ ./data

RUN cargo build

FROM debian:stretch

RUN apt-get update && \
    apt-get install --yes --quiet ca-certificates libssl1.1 && \
    apt-get clean

COPY data/config.toml /etc/nanobot.toml
COPY --from=builder /root/target/debug/nanobot /usr/local/bin/

ENTRYPOINT [ "/usr/local/bin/nanobot" ]