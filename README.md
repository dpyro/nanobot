README
======

Testing Nanobot
---------------

```sh
docker-compose -f docker-compose.test.yml up --build
```

Scraping karma from old logs
----------------------------

```sh
./scripts/scraper.pl [...paths] | ./scripts/redisify.pl | redis-cli --pipe
```